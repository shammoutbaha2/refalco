import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:refalco/src/core/theme/theme.dart';
import 'package:refalco/src/core/widgets/custom_text.dart';
import 'package:refalco/src/features/orders/presentation/manager/orders_controller.dart';
import 'package:refalco/src/features/orders/presentation/widgets/order_card.dart';

import '../../../../core/pagination/pagination_list_view.dart';
import '../../../../core/service/sl.dart';

class OrderScreen extends StatelessWidget {
  OrderScreen({super.key});
  final OrdersController controller = Get.put(sl<OrdersController>());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppTheme.getColorScheme.secondary,
          centerTitle: true,
          title: cusText(text: "Orders", size: 25),
          elevation: 1,
          shadowColor: Colors.grey,
        ),
        // backgroundColor: Colors.white,
        body: RefreshIndicator(
          onRefresh: () => controller.fetch(),
          child: PaginationListView(
            controller: controller,
            size: Get.size,
            itemBuilder: (context, index) {
              return OrderCard(order: controller.data[index]);
            },
          ),
        ));
  }
}
