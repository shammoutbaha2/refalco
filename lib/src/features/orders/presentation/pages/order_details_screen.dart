import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:refalco/src/features/orders/presentation/manager/orders_controller.dart';
import 'package:refalco/src/features/orders/presentation/widgets/map_widget.dart';
import 'package:refalco/src/features/orders/presentation/widgets/order_details_card.dart';

import '../../../../core/constant/const_values.dart';
import '../../../../core/service/sl.dart';
import '../widgets/favorite_button.dart';

class OrderDetailsScreen extends StatelessWidget {
  OrderDetailsScreen({
    super.key,
    required this.orderId,
  });

  final String orderId;
  final OrdersController controller = Get.find<OrdersController>();
  @override
  Widget build(BuildContext context) {
    final order = sl<OrdersController>().findOrderById(orderId);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      body: SizedBox(
        width: ScreenUtil().screenWidth,
        height: ScreenUtil().screenHeight,
        child: Stack(
          children: [
            MapWidget(latlan: LatLng(order.address.lat, order.address.lng)),
            OrderDetailsCard(order: order),
            Positioned(
                top: ScreenUtil().screenHeight * 0.04,
                child: const ArrowButtonBack()),
            Positioned(
                top: ScreenUtil().screenHeight * 0.04,
                right: 20,
                child: FavoriteButton(orderId: order.id))
          ],
        ),
      ),
    );
  }
}

class ArrowButtonBack extends StatelessWidget {
  const ArrowButtonBack({super.key, this.moreFunc});
  final dynamic moreFunc;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        borderRadius: BorderRadius.circular(25),
        onTap: () {
          Navigator.pop(context);
          try {
            moreFunc();
          } catch (_) {}
        },
        child: CircleAvatar(
          backgroundColor: Colors.black.withOpacity(0.3),
          radius: 20,
          child: const Icon(
            IconlyLight.arrowLeft,
            color: Colors.white,
          ),
        ),
      ).animate().slideX(duration: defaultDuration, curve: Curves.easeInOut),
    );
  }
}
