import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/constant/const_values.dart';
import '../../../../core/theme/theme.dart';
import '../../../../core/widgets/custom_text.dart';
import '../../domain/entities/order.dart';

class OrderDetailsCard extends StatefulWidget {
  const OrderDetailsCard({
    super.key,
    required this.order,
  });

  final Order order;

  @override
  State<OrderDetailsCard> createState() => _OrderDetailsCardState();
}

class _OrderDetailsCardState extends State<OrderDetailsCard> {
  bool isOpen = false;
  bool isAnimate = false;

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
        duration: defaultDuration,
        curve: Curves.easeOutBack,
        bottom: 0,
        top: isOpen
            ? ScreenUtil().screenHeight * 0.3
            : ScreenUtil().screenHeight * 0.48,
        child: GestureDetector(
          onTap: () => setState(() {
            isOpen = !isOpen;
          }),
          child: Container(
            padding: const EdgeInsets.all(10),
            width: ScreenUtil().screenWidth,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(widget.order.image),
                    fit: BoxFit.cover,
                    opacity: 0.1),
                color: AppTheme.getColorScheme.secondary,
                borderRadius:
                    const BorderRadius.only(topRight: Radius.circular(50))),
            child: ListView(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: cusText(
                          align: TextAlign.left,
                          text: widget.order.date,
                          color: AppTheme.getColorScheme.onPrimary,
                          overflow: TextOverflow.visible,
                          size: 20.sp),
                    ),
                  ],
                ).animate().slideX(
                    begin: -0.5,
                    end: 0,
                    duration: defaultDurationF,
                    curve: Curves.easeOutBack),
                SizedBox(height: ScreenUtil().screenHeight * 0.04),
                cusText(text: "Price", size: 20, align: TextAlign.left),
                SizedBox(
                  width: ScreenUtil().screenWidth * 0.8,
                  child: cusText(
                      align: TextAlign.left,
                      text: "${widget.order.total} ${widget.order.currency}",
                      color: AppTheme.getColorScheme.onPrimary,
                      overflow: TextOverflow.visible,
                      size: 16.sp),
                ),
                SizedBox(height: ScreenUtil().screenHeight * 0.02),
                cusText(text: "Items", size: 20, align: TextAlign.left),
                SizedBox(height: ScreenUtil().screenHeight * 0.02),
                SizedBox(
                    child: ListView.separated(
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(
                      decelerationRate: ScrollDecelerationRate.fast),
                  itemCount: widget.order.items.length,
                  itemBuilder: (context, index) {
                    final item = widget.order.items[index];
                    return Padding(
                      padding: const EdgeInsets.all(6),
                      child: ListTile(
                        leading: cusText(text: item.id.toString()),
                        title: cusText(text: item.name.toString()),
                        subtitle: cusText(text: item.price.toString()),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) =>
                      Divider(color: AppTheme.getColorScheme.primary),
                )),
              ],
            ),
          )
              .animate()
              .slideX(
                  begin: -0.1,
                  end: 0,
                  duration: defaultDurationVS,
                  curve: Curves.easeOutBack)
              .slideY(
                  begin: 0.1,
                  end: 0,
                  duration: defaultDurationVS,
                  curve: Curves.easeOutBack),
        ));
  }
}
