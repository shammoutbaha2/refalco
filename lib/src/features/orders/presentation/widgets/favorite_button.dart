import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:get/get.dart';

import '../../../../core/theme/theme.dart';
import '../manager/orders_controller.dart';

class FavoriteButton extends StatelessWidget {
  FavoriteButton({super.key, required this.orderId});
  final String orderId;
  final OrdersController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrdersController>(
      init: controller,
      builder: (controller) => IconButton(
          onPressed: () => controller.toggleIsFavorite(orderId),
          icon: Icon(
              controller.findOrderById(orderId).isFavorite
                  ? IconlyBold.heart
                  : IconlyBroken.heart,
              color: AppTheme.getColorScheme.primary)),
    );
  }
}
