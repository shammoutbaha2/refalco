import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../core/functions/print.dart';

class MapWidget extends StatelessWidget {
  const MapWidget({Key? key, required this.latlan}) : super(key: key);
  final LatLng latlan;
  static const defaultZoom = 14.4746;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: Get.height * 0.5,
      width: Get.width,
      child: GoogleMap(
        
        initialCameraPosition: CameraPosition(
          target: latlan,
          zoom: defaultZoom,
        ),
        onMapCreated: (GoogleMapController controller) {},
        onCameraMove: (CameraPosition newPosition) {},
        markers: {Marker(markerId: const MarkerId("value"), position: latlan)},
        mapType: MapType.hybrid,
        myLocationButtonEnabled: true,
        myLocationEnabled: false,
        zoomGesturesEnabled: true,
        padding: const EdgeInsets.all(0),
        buildingsEnabled: true,
        cameraTargetBounds: CameraTargetBounds.unbounded,
        compassEnabled: true,
        indoorViewEnabled: false,
        mapToolbarEnabled: true,
        minMaxZoomPreference: MinMaxZoomPreference.unbounded,
        rotateGesturesEnabled: true,
        scrollGesturesEnabled: true,
        tiltGesturesEnabled: true,
        trafficEnabled: false,
      ),
    );
  }
}
