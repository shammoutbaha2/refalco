import 'package:flutter/material.dart';
import 'package:flutter_iconly/flutter_iconly.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:refalco/src/core/theme/theme.dart';
import 'package:refalco/src/features/orders/domain/entities/order.dart';
import 'package:refalco/src/features/orders/presentation/pages/order_details_screen.dart';
import 'package:refalco/src/features/orders/presentation/manager/orders_controller.dart';
import 'package:refalco/src/features/orders/presentation/widgets/favorite_button.dart';

import '../../../../core/widgets/cached_image.dart';
import '../../../../core/widgets/custom_text.dart';

class OrderCard extends StatelessWidget {
  OrderCard({Key? key, required this.order}) : super(key: key);
  final Order order;
  final OrdersController controller = Get.find<OrdersController>();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
          horizontal: ScreenUtil().screenWidth * 0.01,
          vertical: ScreenUtil().screenWidth * 0.02),
      child: Material(
        color: Colors.transparent,
        elevation: 5,
        borderRadius: const BorderRadius.all(Radius.circular(25)),
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          children: [
            GestureDetector(
              onTap: () => Get.to(() => OrderDetailsScreen(orderId: order.id),
                  transition: Transition.fadeIn),
              child: SizedBox(
                width: ScreenUtil().screenWidth,
                height: ScreenUtil().screenHeight * 0.2,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: cachedImage(order.image, fit: BoxFit.cover)),
              ),
            ),
            Container(
              height: 40,
              width: ScreenUtil().screenWidth,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: <Color>[
                    Color.fromARGB(108, 0, 1, 2),
                    Color.fromARGB(173, 3, 25, 39),
                  ],
                ),
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Center(
                    child: SizedBox(
                        width: ScreenUtil().screenWidth * 0.3,
                        child: cusText(
                            text: order.date, color: Colors.white, size: 18)),
                  ),
                  const Spacer(),
                  cusText(
                      text: "${order.total} ${order.currency}",
                      color: Colors.white,
                      size: 18),
                  const Spacer(),
                  FavoriteButton(orderId: order.id)
                ],
              ),
            ),
            Positioned(
              top: 2,
              left: 5,
              child: Container(
                padding: const EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: const Color.fromARGB(108, 0, 1, 2),
                    borderRadius: BorderRadius.circular(20)),
                child: cusText(text: order.id, color: Colors.white, size: 18),
              ),
            )
          ],
        ),
      ),
    );
  }
}
