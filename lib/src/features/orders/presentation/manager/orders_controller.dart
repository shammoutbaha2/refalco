import 'package:refalco/src/core/errors/error_hdl.dart';
import 'package:refalco/src/core/helper/notification_services.dart';
import 'package:refalco/src/core/helper/storage_helper.dart';
import 'package:refalco/src/core/service/sl.dart';
import 'package:refalco/src/features/orders/domain/usecases/get_orders_uc.dart';

import '../../../../core/pagination/pagination_controller.dart';
import '../../domain/entities/order.dart';
import 'package:audioplayers/audioplayers.dart';

class OrdersController extends PaginationController<Order> {
  final GetOrdersUC getOrdersUC;
  OrdersController({required this.getOrdersUC});

  @override
  Future<List<Order>> apiCall() async {
    final either = await getOrdersUC(page: super.pageNumber);
    List<Order> list = [];
    either.fold((l) => throw handleErrByType(l), (r) => list = r);
    return list;
  }

  void toggleIsFavorite(String id) async {
    final List<String> listOfFavorites = sl<StorageHelper>().getListOfString("favorites")??[];

    final order = super.data.firstWhere((element) => element.id == id);
    order.isFavorite = !order.isFavorite;
    if (order.isFavorite) {
      listOfFavorites.add(order.id);
      sl<NotificationServices>()
          .showNotification("Refalcoo!", "Favorite Added Successfully");
    } else {
      listOfFavorites.add(order.id);
      AudioPlayer audioPlayer = AudioPlayer();
      await audioPlayer.play(AssetSource("audios/audio.mp3"));
    }
   await sl<StorageHelper>().setAllString("favorites" , listOfFavorites);
    update();
  }

  Order findOrderById(String id) {
    return super.data.firstWhere((element) => element.id == id);
  }
}
