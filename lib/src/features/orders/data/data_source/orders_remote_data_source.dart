import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:refalco/src/core/functions/print.dart';
import 'package:refalco/src/core/helper/dio_helper.dart';

import '../../../../core/constant/const_values.dart';
import '../../../../core/errors/error_hdl.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/service/sl.dart';
import '../models/order.dart';
import 'orders_local_data_source.dart';

abstract class OrdersRemoteDataSource {
  Future<List<OrderModel>> getOrders({required int page});
}

class OrdersRemoteDataSourceImpl extends OrdersRemoteDataSource {
  final DioHelper client;

  OrdersRemoteDataSourceImpl(this.client);
  @override
  Future<List<OrderModel>> getOrders({required int page}) async {
    try {
      const path = 'orders';
      final response = await client.get(
          path: path,
          queryParameters: {"page": page, "limit": 15},
          auth: false);

      if (response.statusCode == 200) {
        sl<OrdersLocalDataSource>().dropTable();
        sl<OrdersLocalDataSource>().insert(jsonEncode(response.data["data"]));

        final responseModel = OrderModel.listOfModel(response.data["data"]);
        return responseModel;
      } else {
        throw anErrorOccurred;
      }
    } on DioError catch (e) {
      pr(e.error);
      throw ServerException(handleError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }
}
