import 'package:refalco/src/core/errors/exceptions.dart';

import '../../../../core/helper/local_storage_helper.dart';
import 'package:refalco/src/core/constant/const_values.dart';
import 'package:refalco/src/core/errors/failures.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;

class OrdersLocalDataSource extends DBHelper {
  final String name = "Orders1";

  static sql.Database? _database;

  @override
  Future initDatabase() async {
    if (_database != null) return;

    await database();
  }

  @override
  Future database() async {
    String dbPath = await sql.getDatabasesPath();
    _database = await sql.openDatabase(path.join(dbPath, 'orders.db'),
        onCreate: (sql.Database db, int version) {
      return db.execute('CREATE TABLE Orders1 (data Text)');
    }, version: 1);
  }

  @override
  Future insert(dynamic data) async {
    try {
      await initDatabase();
      await _database!.insert(name, {"data": data},
          conflictAlgorithm: sql.ConflictAlgorithm.replace);
    } catch (e) {
      throw CacheFailure(anErrorOccurred);
    }
  }

  @override
  Future<List<Map<String, dynamic>>> getData() async {
    try {
      await initDatabase();
      return await _database!.query(name);
    } catch (e) {
      throw CacheException(e.toString());
    }
  }

  @override
  Future<int> delete(int id) async {
    await initDatabase();

    return await _database!.delete(name, where: 'id = ?', whereArgs: [id]);
  }

  @override
  Future<int> update(Map<String, dynamic> data) async {
    if (_database == null) {
      await initDatabase();
    }
    return await _database!
        .update(name, data, where: 'id = ?', whereArgs: [data['id']]);
  }

  Future dropTable() async {
    try {
      await initDatabase();
      await _database!.delete(" Orders1");
    } catch (e) {
      print(e);
    }
  }
}
