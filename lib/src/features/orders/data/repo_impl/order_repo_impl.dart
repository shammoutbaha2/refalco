import 'dart:convert';

import 'package:refalco/src/core/constant/const_values.dart';
import 'package:refalco/src/core/errors/failures.dart';

import 'package:dartz/dartz.dart' hide Order;
import 'package:refalco/src/core/functions/check_connection.dart';
import 'package:refalco/src/core/functions/print.dart';
import 'package:refalco/src/core/widgets/scaffold_messenger.dart';
import 'package:refalco/src/features/orders/data/data_source/orders_remote_data_source.dart';
import 'package:refalco/src/features/orders/presentation/manager/orders_controller.dart';

import '../../../../core/errors/error_hdl.dart';
import '../../../../core/errors/exceptions.dart';
import '../../../../core/service/sl.dart';
import '../../domain/repo/orders_repo.dart';

import '../../domain/entities/order.dart';
import '../data_source/orders_local_data_source.dart';
import '../models/order.dart';

class OrdersRepoImpl extends OrdersRepo {
  final OrdersRemoteDataSource dataSource;

  OrdersRepoImpl({required this.dataSource});
  @override
  Future<Either<Failure, List<Order>>> getOrders({required int page}) async {
    if (await noConnection()) {
      toastErrorMessage(handleErrByType(NetworkFailure(noInternetConnection)));
      return right(await getSavedOrder());
    }
    try {
      final result = await dataSource.getOrders(page: page);
      return Right(result);
    } on CacheException catch (e) {
      toastErrorMessage(handleErrByType(CacheFailure(e.message)));
      return right(await getSavedOrder());
    } on ServerException catch (e) {
      toastErrorMessage(handleErrByType(ServerFailure(e.message)));
      return right(await getSavedOrder());
    } catch (e) {
      toastErrorMessage("Unkown Failure");
      return right(await getSavedOrder());
    }
  }

  Future<List<OrderModel>> getSavedOrder() async {
    try {
      if (sl<OrdersController>().getData.isNotEmpty) return [];
      List a = await sl<OrdersLocalDataSource>().getData();
      return OrderModel.listOfModel(jsonDecode(a[0]["data"]));
    } on CacheException catch (e) {
      throw CacheFailure(e.message);
    } catch (e) {
      throw UnknownFailure(e.toString());
    }
  }
}
