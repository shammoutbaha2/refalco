import '../../domain/entities/address.dart';

class AddressModel extends Address {
  AddressModel({required super.lat, required super.lng});

  factory AddressModel.fromJson(Map<String, dynamic> json) => AddressModel(
      lat: double.parse(json["lat"]), lng: double.parse(json["lng"]));

  static Map<String, dynamic> toJson(Address address) {
    return {"lat": address.lat, "lng": address.lng};
  }
}
