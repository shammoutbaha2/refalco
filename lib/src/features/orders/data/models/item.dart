import '../../domain/entities/item.dart';

class ItemModel extends Item { 
  ItemModel({
   required super.id,required super.name,required super.price});

   factory ItemModel.fromJson(Map<String ,dynamic> json)=>ItemModel(id: json["id"], name: json["name"], price: json["price"]);

   
  static Map<String,dynamic> toJson(Item item){
    return {
      "id":item.id , 
      "name":item.name , 
      "price":item.price , 
    };
  }

}
