import 'package:refalco/src/core/helper/storage_helper.dart';
import 'package:refalco/src/core/service/sl.dart';

import '../../domain/entities/order.dart';
import 'address.dart';
import 'item.dart';
import 'package:intl/intl.dart';

class OrderModel extends Order {
  OrderModel(
      {required super.id,
      required super.total,
      required super.date,
      required super.image,
      required super.currency,
      required super.address,
      required super.isFavorite,
      required super.items});
  factory OrderModel.fromJson(Map<String, dynamic> json) {
    return OrderModel(
      id: json["id"],
      total: json["total"],
      date: DateFormat('yyyy-MM-dd').format(DateTime.parse(json["created_at"])),
      image: json["image"],
      currency: json["currency"],
      address: AddressModel.fromJson(json["address"]),
      isFavorite: checkIfHasFav(json["id"]),
      items: List<ItemModel>.from(
          (json["items"] as List).map((e) => ItemModel.fromJson(e))),
    );
  }

  static List<OrderModel> listOfModel(List list) {
    return list.map((e) => OrderModel.fromJson(e)).toList();
  }

  static Map<String, dynamic> toJson(OrderModel order) {
    return {
      "id": order.id,
      "total": order.total,
      "date": order.date,
      "image": order.image,
      "currency": order.id,
      "address": AddressModel.toJson(order.address),
      "items": order.items
          .map<Map<String, dynamic>>((e) => ItemModel.toJson(e))
          .toList(),
    };
  }

  static bool checkIfHasFav(String id) {
    final List<String?>? list =
        sl<StorageHelper>().getListOfString("favorites");
    if (list == null) return false;
    return list.contains(id);
  }
}
