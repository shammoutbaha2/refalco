import 'package:dartz/dartz.dart' hide Order;
import 'package:refalco/src/core/errors/failures.dart';

import '../entities/order.dart';

abstract class OrdersRepo {
  Future<Either<Failure, List<Order>>> getOrders({required int page});
}
