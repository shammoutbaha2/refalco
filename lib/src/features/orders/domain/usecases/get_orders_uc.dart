import 'package:dartz/dartz.dart'hide Order;
import 'package:refalco/src/features/orders/domain/repo/orders_repo.dart';

import '../../../../core/errors/failures.dart';
import '../entities/order.dart';

class GetOrdersUC {
  final OrdersRepo repo;

  GetOrdersUC({required this.repo});

  Future<Either<Failure, List<Order>>> call({required int page})async{
    return await repo.getOrders(page: page);
  }

}
