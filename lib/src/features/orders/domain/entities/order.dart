import 'address.dart';
import 'item.dart';

class Order {
  final String id;
  final String total;
  final String date;
  final String image;
  final String currency;
  final Address address;
  final List<Item> items;
  bool isFavorite;

  Order(
      {required this.id,
      required this.total,
      required this.date,
      required this.image,
      required this.currency,
      required this.address,
      required this.items,
      required this.isFavorite});


}
