class Item {
  final int id;
  final String name;
  final String price;

  Item({required this.id, required this.name, required this.price});

}
