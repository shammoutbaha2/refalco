class Address {
  final double lat;
  final double lng;

  Address({required this.lat, required this.lng});

}
