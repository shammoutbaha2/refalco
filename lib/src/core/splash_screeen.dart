import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:refalco/src/features/orders/presentation/pages/orders_screen.dart';

import 'constant/const_values.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future.delayed(defaultDurationVS, () {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => OrderScreen()));
    });
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 120.w,
            child: Image.asset("assets/images/order.png")),
      ).animate().fadeIn(duration: defaultDuration, begin: 0),
    );
  }
}
