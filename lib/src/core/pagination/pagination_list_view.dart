import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:refalco/src/core/constant/const_values.dart';
import 'package:refalco/src/core/functions/future_builder_hdl.dart';
import 'package:refalco/src/core/pagination/pagination_controller.dart';
import 'package:refalco/src/core/widgets/custom_text.dart';

class PaginationListView extends StatelessWidget {
  const PaginationListView(
      {super.key,
      required this.controller,
      required this.size,
      this.loaderLottie,
      this.noDataLottie,
      required this.itemBuilder});

  final PaginationController controller;
  final Size size;
  final Widget Function(BuildContext, int) itemBuilder;
  final String? noDataLottie;
  final String? loaderLottie;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: controller.fetch(),
        builder: (context, snapshot) {
          return futureBuilderHandler(
              snapshot,
              GetBuilder<PaginationController>(
                  init: controller,
                  builder: (controller) {
                    if (controller.hasError) {
                      return Center(child: cusText(text: anErrorOccurred));
                    } else if (controller.data.isEmpty) {
                      return Center(child: cusText(text: "Nothing to show"));
                    }
                    return Column(
                      children: [
                        Expanded(
                            child: ListView.builder(
                          controller: controller.scrollController,
                          padding: const EdgeInsets.all(15),
                          itemBuilder: itemBuilder,
                          itemCount: controller.data.length,
                        )),
                        Visibility(
                            visible: controller.loader && !controller.isFinish,
                            child: const Center(
                              child: CircularProgressIndicator(),
                            ))
                      ],
                    );
                  }));
        });
  }
}
