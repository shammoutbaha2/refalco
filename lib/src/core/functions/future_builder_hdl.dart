import 'package:flutter/material.dart';

import '../widgets/custom_text.dart'; 

Widget futureBuilderHandler(AsyncSnapshot snapshot, Widget widget , {Widget? err , Widget? waiting}) {
  return (snapshot.connectionState == ConnectionState.waiting)
      ? waiting?? const Center(child: CircularProgressIndicator())
      : snapshot.hasError
          ? err?? Center(
              child: cusText(text: snapshot.error.toString()))
          : widget;
}
