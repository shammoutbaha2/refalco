import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../theme/theme.dart';

// ignore: must_be_immutable
class CusSizedText extends StatelessWidget {
  CusSizedText(
      {Key? key,
      required this.text,
      this.color,
      required this.width,
      this.maxLines,
      this.size = 15,
      this.overflow = TextOverflow.visible,
      this.fontFamily,
      this.align = TextAlign.center,
      this.weight = FontWeight.w500})
      : super(key: key);

  Color? color;
  final String text;
  final double width;
  int? maxLines;
  double size;
  TextAlign align;
  FontWeight weight;
  TextOverflow overflow;
  String? fontFamily;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: ScreenUtil().scaleWidth * width,
      child: cusText(
          text: text,
          color: color ?? AppTheme.getColorScheme.primary,
          maxLines: maxLines,
          fontFamily: fontFamily,
          align: align,
          overflow: overflow,
          size: size,
          weight: weight),
    );
  }
}

Text cusText(
    {required String text,
    Color? color,
    double size = 14,
    int? maxLines,
    overflow = TextOverflow.ellipsis,
    String? fontFamily,
    align = TextAlign.center,
    weight = FontWeight.w500}) {
  return Text(
    text,
    maxLines: maxLines,
    textAlign: align,
    style: TextStyle(
      overflow: overflow,
      color: color ?? AppTheme.getColorScheme.primary,
      fontFamily: fontFamily,
      fontSize: size,
      fontWeight: weight,
    ),
  );
}
