import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'custom_text.dart';

Widget cusShortElevatedButton(
    {required Color color,
    required String title,
    required func,
    double? elevatoin}) {
  ButtonStyle elevatedButton = ElevatedButton.styleFrom(
    backgroundColor: color,
    elevation: elevatoin ?? 5,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
  );
  return ElevatedButton(
    style: elevatedButton,
    onPressed: func,
    child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 25),
        child: cusText(
          text: title,
          color: const Color.fromARGB(255, 255, 255, 255),
          // size: ,
          overflow: TextOverflow.visible,
          weight: FontWeight.w500,
        )),
  );
}

Widget cusLongElevatedButton({
  required Color color,
  required String title,
  required func,
}) {
  ButtonStyle elevatedButton = ElevatedButton.styleFrom(
    backgroundColor: color,
    elevation: 5,
    shape: RoundedRectangleBorder(
        side: BorderSide(color: color),
        borderRadius: BorderRadius.circular(10)),
  );
  return ElevatedButton(
    style: elevatedButton,
    onPressed: func,
    child: SizedBox(
      width: ScreenUtil().screenWidth * 0.7,
      height: 50,
      child: Center(
        child: cusText(
            text: title,
            color: Colors.white,
            size: 18,
            weight: FontWeight.w500),
      ),
    ),
  );
}
