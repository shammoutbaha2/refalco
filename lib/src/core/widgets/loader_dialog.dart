import 'package:flutter/material.dart';

import 'custom_text.dart';
 

class LoaderUtils {
  late BuildContext context;

  LoaderUtils(this.context);

  // this is where you would do your fullscreen loading
  Future<void> startLoading([bool? child]) async {
    return await showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () => Future.value(false),
          child: SimpleDialog(
            elevation: 0.0,
            backgroundColor:
                Colors.transparent, // can change this to your prefered color
            children: <Widget>[
              Center(
                child: child == null
                    ? const CircularProgressIndicator()
                    : const SizedBox(),
              )
            ],
          ),
        );
      },
    );
  }

  Future<void> stopLoading() async {
    Navigator.of(context).pop();
  }

  Future<void> showError(String error) async {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
          action: SnackBarAction(
            label: 'dismiss',
            onPressed: () {
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
            },
          ),
          backgroundColor: Colors.red,
          content: cusText(text: error, color: Colors.white, size: 12)),
    );
  }

  Future showLoaderAndDoSomething(doSomething, {doSomethingAfterLoader}) async {
    try {
      startLoading();
      await doSomething();
      stopLoading();
      doSomethingAfterLoader;
    } catch (e) {
      stopLoading();
      showError(e.toString());
    }
  }
}
