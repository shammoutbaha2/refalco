import 'package:get_it/get_it.dart';
 
import 'init_services.dart';
import 'orders_services.dart';

final sl = GetIt.instance;

Future<void> initSL() async {
  await initCore(sl); 
  await initOrders(sl); 
}
