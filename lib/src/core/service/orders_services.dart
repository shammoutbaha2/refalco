import 'package:get_it/get_it.dart';
import 'package:refalco/src/features/orders/data/data_source/orders_local_data_source.dart';
import 'package:refalco/src/features/orders/data/data_source/orders_remote_data_source.dart';
import 'package:refalco/src/features/orders/domain/repo/orders_repo.dart';

import '../../features/orders/data/repo_impl/order_repo_impl.dart';
import '../../features/orders/domain/usecases/get_orders_uc.dart';
import '../../features/orders/presentation/manager/orders_controller.dart';

Future<void> initOrders(GetIt sl) async {
  // Data Sources
  sl.registerLazySingleton<OrdersLocalDataSource>(
    () => OrdersLocalDataSource(),
  );
  sl.registerLazySingleton<OrdersRemoteDataSource>(
    () => OrdersRemoteDataSourceImpl(sl()),
  );

  // Use Cases
  sl.registerLazySingleton<GetOrdersUC>(() => GetOrdersUC(repo: sl()));

  // Repository
  sl.registerLazySingleton<OrdersRepo>(() => OrdersRepoImpl(dataSource: sl()));

  // controller
  sl.registerLazySingleton<OrdersController>(
      () => OrdersController(getOrdersUC: sl()));
}
