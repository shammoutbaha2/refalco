import 'package:dio/dio.dart';

import '../constant/api.dart';
import '../functions/print.dart';
import 'storage_helper.dart';

abstract class DioHelper {
  Future<Response> get({
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  });

  Future<Response> post({
    required String path,
    FormData? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  });
  Future<Response> update({
    required String path,
    FormData? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  });
  Future<Response> delete({
    required String path,
    FormData? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  });
}

class DioHelperImpl extends DioHelper {
  final Dio client;
  final StorageHelper storage;

  DioHelperImpl({
    required this.client,
    required this.storage,
  });

  @override
  Future<Response> get({
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  }) async { 
    final response = await client.get(
      baseURL + path,
      queryParameters: queryParameters,
      options:
          Options(headers: getHeaders(extraHeaders: extraHeaders, auth: auth)),
    );
    return response;
  }

  @override
  Future<Response> post({
    required String path,
    FormData? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  }) async {
    final response = await client.post(
      baseURL + path,
      data: body,
      queryParameters: queryParameters,
      options:
          Options(headers: getHeaders(extraHeaders: extraHeaders, auth: auth)),
    );
    return response;
  }

  @override
  Future<Response> update(
      {required String path,
      FormData? body,
      Map<String, dynamic>? queryParameters,
      Map<String, String>? extraHeaders,
      bool auth = true}) async {
    final response = await client.put(
      baseURL + path,
      data: body,
      queryParameters: queryParameters,
      options:
          Options(headers: getHeaders(extraHeaders: extraHeaders, auth: auth)),
    );
    return response;
  }

  @override
  Future<Response> delete(
      {required String path,
      FormData? body,
      Map<String, dynamic>? queryParameters,
      Map<String, String>? extraHeaders,
      bool auth = true}) async {
    final response = await client.delete(
      baseURL + path,
      data: body,
      queryParameters: queryParameters,
      options:
          Options(headers: getHeaders(extraHeaders: extraHeaders, auth: auth)),
    );
    return response;
  }

  Map<String, String> getHeaders(
      {Map? extraHeaders, bool auth = false, bool hasImages = false}) {
    return {
      "Accept": hasImages ? "multipart/form-data" : "application/json",
      if (auth) "Authorization": "Bearer $token",
      ...?extraHeaders,
    };
  }

  String get baseURL {
    return api;
  }

  String get token {
    try {
      return storage.getString("token") ?? "";
    } catch (e) {
      return "";
    }
  }
}

















































































































////////////////////////////////////////////////////////////////////////////
///
///




// var request =
//         http.MultipartRequest('POST', Uri.parse('${EndPoints.productApi}'));
//      request.headers.addAll(getHeader());
//     request.fields.addAll( data);

//     if (img != null) {
//       request.files.add(await http.MultipartFile.fromPath('images', img,
//           contentType: MediaType('image', 'jpeg')));
//     }
//     http.StreamedResponse response = await request.send();

//     Map<String, dynamic> json =
//         jsonDecode(await response.stream.bytesToString());
//     if (json['success'] && (json['status'] == 201 || json['status'] == 200)) {
//       ProductModel models = ProductModel.fromJson(json['data']);
//       return models;
//     } else if (json['success'] == false && json['userMessage'] != null) {
//       throw json['userMessage'];
//     } else {
//       throw "خطأ أعد المحاولة في وقت لاحق";
//     }
