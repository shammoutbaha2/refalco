

abstract class DBHelper {
  Future initDatabase();

  Future database();

  Future insert(dynamic data); 

  Future<List<Map<String, dynamic>>> getData();

  Future<int> delete(int id);

  Future<int> update(Map<String, dynamic> data);
}
