import 'package:dio/dio.dart';
import 'package:refalco/src/core/functions/print.dart';

import 'failures.dart';

String handleError(DioError e) {
  if (e.type == DioErrorType.connectionTimeout) {
    return "Connection Time Out Try Again Later";
  } else if (e.type == DioErrorType.receiveTimeout) {
    return "Receive Time Out Try Again Later";
  } else if (e.type == DioErrorType.badResponse) {
    pr(e.response?.data);
    return (e.response?.data is Map) ? e.response?.data["message"] : e.message;
  } else if (e.type == DioErrorType.sendTimeout) {
    return "Send Time Out Try Again Later";
  } else {
    return "Unknown Error";
  }
}

String handleErrByType(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return failure.message;
    case NetworkFailure:
      return "Network Error";
    case CacheFailure:
      return "Cache Error";

    default:
      return "Unknown Error";
  }
}
