import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:oktoast/oktoast.dart';
import 'package:refalco/src/core/helper/notification_services.dart';
import 'package:refalco/src/core/service/sl.dart';
import 'package:refalco/src/core/splash_screeen.dart';
import 'package:refalco/src/core/theme/theme.dart';
import 'package:refalco/src/features/orders/presentation/pages/orders_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initSL();
 

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(392, 834),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) => OKToast(
        child: GetMaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Refalco',
          theme: AppTheme.base(),
          home: SplashScreen(),
        ),
      ),
    );
  }
}
